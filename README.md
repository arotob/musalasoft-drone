## MUSALA SOFT DRONE SYSTEM

**START**

### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---

### Task Solution

The task solution is divided into 2 seperate project. The first project is the REST API and the second is the Drone dispatch controllers application.

### REST API PROJECT

The project is developed in JAVA SpringBoot https://spring.io. SpringBoot allows me to be able to create a microwebservice that can be launched without having to install a seperate server. Other dependencies of the project are

- H2 in memory database to store the drone and medication information
- HATEOAS which is used to build REST Application architecture.
- JPA (Java Persistence API) which is use to map from the H2 database to my application model.

Gradle package manager is used to compile the project.

## Building and Running the Projecct

The project can be built and run using the gradle command './gradlew --configure-on-demand -x check bootRun' in the '/MusalaSoft/MusalaSoftDroneRestService' directory.

## REST API ENDPOINT

The REST API contains the followind endpoint that can be accessed from this url http://localhost:8080.

- [GET] /medication/alls
  : This endpoint returns all the medications information from the database in JSON back to the client
- [GET] /drone/alls
  : This endpoint returns all the drones information from the database in JSON back to the client

- [GET] /dronemedication/{id}
  : This endpoint accept the drone id as parameter and returns all the the medications on the drone in JSON.

- [GET] /medicationdrone/{id}
  : This endpoint accept medication id as parameter and returns all the the drones with the medication in JSON.

- [GET] /availabledrone/{id}
  : This endpoint accept medication id as parameter and returns all the the drones with available weight to carry the medication in JSON.

- [GET] /drone/{id}
  : This endpoint accept drone id as parameter and returns the information of the drone in JSON.

- [POST] /newdrone
  : This endpoint is to add new drone to the database. it accept drone name, serial number, drone model, drone weight, battery time span as parameter and returns if the operation is successful or failed in JSON.

- [POST] /newmed
  : This endpoint is to add new medication to the database. it accept medication name, weight, code, image as parameter and returns if the operation is successful or failed in JSON.

- [POST] /drone
  : This endpoint is to add new medication to a drone. it accept drone id, med id, medication name, medication weight, medication code and medication image url as parameter and returns if the operation is successful or failed in JSON.

- [PUT] /drone
  : This endpoint is to change the state of the drone. It accept drone id and state as parameter and return if the operation is succesfull in JSON.

- [PUT] /dronerecharge
  : This endpoint is to recharge the drone battery. It accept drone id as parameter and return if the operation is succesfull in JSON.

- [DELETE] /deleteAllMedication/{id}
  : This endpoint is to delete all medication assigned to a particular drone. It accept drone id as parameter and return if the operation is succesfull in JSON.

- [DELETE] /deleteAllMedicationDrone/{id}
  : This endpoint is to delete a particular medication assigned to a any drone. It accept medication id as parameter and return if the operation is succesfull in JSON.

- [DELETE] /deleteDroneInformation/{id}
  : This endpoint is to delete a drone from the database. It accept drone id as parameter and return if the operation is succesfull in JSON.

- [DELETE] /deleteMedicationInformation/{id}
  : This endpoint is to delete a medication from the database. It accept medication id as parameter and return if the operation is succesfull in JSON.

### DRONE DISPATCH CONTROLLER APPLICATION PROJECT

The project is developed in JAVA with gradle package manager. Other dependencies of the project are

- retrofit is a type-safe HTTP client for Android and Java.
- Google JSON Code allows JSON response to be serialized into a model

Gradle package manager is used to compile the project.

## Building and Running the Projecct

The project can be built and run using the gradle command '../gradlew --configure-on-demand -x check run' in the '/MusalaSoft/MusalaSoftDroneApp/app' directory.

**Note**
The REST API Project needs to be started before the Drone Dispatch Application
