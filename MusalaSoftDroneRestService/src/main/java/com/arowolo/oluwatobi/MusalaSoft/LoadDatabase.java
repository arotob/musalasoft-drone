/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft;


import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Drones;
import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Medications;
import com.arowolo.oluwatobi.MusalaSoft.Enum.droneModel;
import com.arowolo.oluwatobi.MusalaSoft.Enum.stateEnum;
import com.arowolo.oluwatobi.MusalaSoft.Interface.DrioneInterface;
import com.arowolo.oluwatobi.MusalaSoft.Interface.MedicationInterface;
import java.util.Date;
import org.springframework.context.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author arotob
 */
@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
    
    @Bean
    CommandLineRunner initDatabase (MedicationInterface medinterface, DrioneInterface droneinterface)
    {
        return (args) -> {
             
             log.info("Preloading " + medinterface.save(new Medications("Atorvastatin", 30, "ATO", "https://c8.alamy.com/comp/DF9JF3/atorvastatin-tablets-DF9JF3.jpg")));
             log.info("Preloading " + medinterface.save(new Medications("Levothyroxine", 50, "LEV", "https://healthjade.net/wp-content/uploads/2019/04/Levothyroxine.jpg")));
             log.info("Preloading " + medinterface.save(new Medications("Lisinopril", 20, "LIS", "https://5.imimg.com/data5/SELLER/Default/2020/10/TC/IN/IF/26192048/lisinopril-tablet-500x500.jpg")));
             
             java.util.Date now = new Date();
            
             log.info("Preloading " + droneinterface.save(new Drones("DJI Mini 2", "DJI23C391", droneModel.Cruiserweight, 50, 10, now, stateEnum.IDLE)));
             log.info("Preloading " + droneinterface.save(new Drones("Ryze Tello", "RYZ723LL1", droneModel.Heavyweight, 20, 25, now, stateEnum.LOADING)));
             log.info("Preloading " + droneinterface.save(new Drones("DJI Air 2S", "DJI2331DS", droneModel.Middleweight, 75, 15, now, stateEnum.RETURNING)));
             
             
             
        };
    }
    
}
