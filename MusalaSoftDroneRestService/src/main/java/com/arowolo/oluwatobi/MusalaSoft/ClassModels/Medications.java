/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft.ClassModels;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author arotob
 */
@Entity
public class Medications {
    private @Id @GeneratedValue Long id;
    private String name;
    private int weight;
    private String code;
    private String image_url;
    
    public Medications()
    {
        
    }
    
    public Medications(String name, int weight, String code, String image_url)
    {
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.image_url = image_url;
        
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    public void setWeight(int weight)
    {
        this.weight = weight;
    }
    
    public void setCode(String code)
    {
        this.code = code;
        
    }
    public void setImageUrl(String image_url)
    {
        this.image_url = image_url;
        
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public String getName()
    {
        return this.name;
    }
    public int getWeight()
    {
        return this.weight;
    }
    public String getCode()
    {
        return this.code;
    }
    
    public String getImageUrl()
    {
        return this.image_url;
    }
    
    
    
}
