/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft.ClassModels;

import com.arowolo.oluwatobi.MusalaSoft.Enum.droneModel;
import com.arowolo.oluwatobi.MusalaSoft.Enum.stateEnum;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author arotob
 */
@Entity
public class Drones {
    private @Id @GeneratedValue Long id;
    private String droneName;
    private String droneSerialNumber;
    private droneModel droneModel;
    private int droneWeight;
    private int batteryTimeLimit;
    private java.util.Date lastRecharge;
    private stateEnum droneState;
    
   
    
    public Drones() {
    }
    
    public Drones(String drone_name, String drone_serial_number, droneModel drone_model, int drone_weight, int battery_time_limit, java.util.Date last_recharge, stateEnum drone_state)
    {
        this.droneName = drone_name;
        this.droneSerialNumber = drone_serial_number;
        this.droneModel = drone_model;
        this.droneWeight = drone_weight;
        this.batteryTimeLimit = battery_time_limit;
        this.lastRecharge = last_recharge;
        this.droneState = drone_state;
        
    }
    
    public void setDroneName(String drone_name)
    {
        this.droneName = drone_name;
    }
    
    public void setDroneSerialNumber (String drone_serial_number)
    {
        this.droneSerialNumber = drone_serial_number;
    }
    
    public void setDroneModel (droneModel drone_model)
    {
        this.droneModel = drone_model;
    }
    
    public void setDroneWeight (int drone_weight)
    {
        this.droneWeight = drone_weight;
    }
    public void setBatteryTimeLime(int battery_time_limit)
    {
        this.batteryTimeLimit = battery_time_limit;
    }
    public  void setLastRecharge (java.util.Date last_recharge)
    {
        this.lastRecharge = last_recharge;
    }
    public  void setDroneState (stateEnum drone_state)
    {
        this.droneState = drone_state;
    }
    
    
    public Long getId()
    {
        return this.id;
    }
    
    public String getDroneName()
    {
        return this.droneName;
    }
    public String getDroneSerialNumber()
    {
        return this.droneSerialNumber;
    }
    public droneModel getDroneModel()
    {
        return this.droneModel;
    }
    public int getDroneWeight()
    {
        return this.droneWeight;
    }
    public int getBatteryTimeLimit()
    {
        return  this.batteryTimeLimit;
    }
    
    public java.util.Date getLastRecharge()
    {
        return this.lastRecharge;
        
    }
    public stateEnum getDroneState()
    {
        return this.droneState;
    }
    
    
    
    
}
