/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft;
import com.arowolo.oluwatobi.MusalaSoft.ClassModels.DroneMed;
import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Drones;
import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Medications;
import com.arowolo.oluwatobi.MusalaSoft.Enum.droneModel;
import com.arowolo.oluwatobi.MusalaSoft.Enum.stateEnum;
import com.arowolo.oluwatobi.MusalaSoft.Interface.DrioneInterface;
import com.arowolo.oluwatobi.MusalaSoft.Interface.DroneMedInterface;
import com.arowolo.oluwatobi.MusalaSoft.Interface.MedicationInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author arotob
 */
@RestController
public class RestServiceController {
    
 private MedicationInterface medinterface;
 private DrioneInterface droneinterface;
 private DroneMedInterface dronemedinterface;
 
 
  public RestServiceController(MedicationInterface medinterface, DrioneInterface droneinterface, DroneMedInterface dronemedinterface) {
       
        this.medinterface = medinterface;
        this.droneinterface = droneinterface;
        this.dronemedinterface = dronemedinterface;
    }
  
  
    @GetMapping("/medication/alls")
    String getAllMedication()
    {
      
        List<Medications> medicationlist = medinterface.findAll();
        JSONArray jsonResult = new JSONArray();
        
        if (!medicationlist.isEmpty())
        {
            for (Medications meditem: medicationlist)
            {
                JSONObject jobject = new JSONObject();
                jobject.put("command", "success");
                jobject.put("id", meditem.getId());
                jobject.put("name", meditem.getName());
                jobject.put("weight", meditem.getWeight());
                jobject.put("code", meditem.getCode());
                jobject.put("image_url", meditem.getImageUrl());
                
                jsonResult.add(jobject);


            }
            
            
        }
        else
        {
             JSONObject jobject = new JSONObject();
                jobject.put("command", "failed");
                 jsonResult.add(jobject);
        }
            
        
        return  jsonResult.toString();
    }
    
     @GetMapping("/drone/alls")
    String getAllDrone()
    {
      
        List<Drones> dronelist = droneinterface.findAll();
        JSONArray jsonResult = new JSONArray();
        
        if (!dronelist.isEmpty())
        {
            for (Drones droneitem: dronelist)
            {
                JSONObject jobject = new JSONObject();
                jobject.put("command", "success");
                jobject.put("id", droneitem.getId());
                jobject.put("name", droneitem.getDroneName());
                jobject.put("serial_number", droneitem.getDroneSerialNumber());
                jobject.put("model", droneitem.getDroneModel());
                jobject.put("weight", droneitem.getDroneWeight());
               
                
                int usedWeight = checkDroneWeight(droneitem.getId().intValue());
                jobject.put("used_weight", usedWeight);
               
                jobject.put("state", droneitem.getDroneState());
                
                jobject.put("battery_timespan", droneitem.getBatteryTimeLimit());
                
                int capacity = getTimeDiff(droneitem.getLastRecharge().toString(), droneitem.getBatteryTimeLimit());
                if (capacity <= 0)
                {
                    capacity = 0;
                }
                jobject.put("battery_capacity", capacity);
                
                
                jsonResult.add(jobject);


            }
            
            
        }
        else
        {
             JSONObject jobject = new JSONObject();
                jobject.put("command", "failed");
                 jsonResult.add(jobject);
        }
            
        
        return  jsonResult.toString();
    }
    
    @GetMapping("/dronemedication/{id}")
    String getAllDroneMedication(@PathVariable int id)
    {
        List<DroneMed> myDrone = dronemedinterface.findByDroneId(id);
     
        JSONArray jsonResult = new JSONArray();
        
        if (!myDrone.isEmpty())
        {
            for (DroneMed dronemeditem: myDrone)
            {
                JSONObject jobject = new JSONObject();
               
                jobject.put("id", dronemeditem.getId());
                jobject.put("name", dronemeditem.getName());
                jobject.put("weight", dronemeditem.getWeight());
                jobject.put("code", dronemeditem.getCode());
                jobject.put("image_url", dronemeditem.getImageUrl());
                
                jsonResult.add(jobject);


            }
            
            
        }
        else
        {
             JSONObject jobject = new JSONObject();
                jobject.put("command", "empty");
                 jsonResult.add(jobject);
        }
            
        
        return  jsonResult.toString();
    }
    
      @GetMapping("/medicationdrone/{id}")
    String getAllMedicationDrone(@PathVariable int id)
    {
        List<DroneMed> myMed = dronemedinterface.findByMedId(id);
     
        JSONArray jsonResult = new JSONArray();
        
        if (!myMed.isEmpty())
        {
            for (DroneMed meditem: myMed)
            {
                
                Optional<Drones> droneItem = droneinterface.findById(Long.valueOf(meditem.getDroneId()));
  
                
               JSONObject jobject = new JSONObject();
              
                jobject.put("id", droneItem.get().getId());
                jobject.put("name", droneItem.get().getDroneName());
                jobject.put("serial_number", droneItem.get().getDroneSerialNumber());
                jobject.put("model", droneItem.get().getDroneModel());
                jobject.put("weight", droneItem.get().getDroneWeight());
               
                
                int usedWeight = checkDroneWeight(droneItem.get().getId().intValue());
                jobject.put("used_weight", usedWeight);
               
                jobject.put("state", droneItem.get().getDroneState());
                
                jobject.put("battery_timespan", droneItem.get().getBatteryTimeLimit());
                
                int capacity = getTimeDiff(droneItem.get().getLastRecharge().toString(), droneItem.get().getBatteryTimeLimit());
                if (capacity <= 0)
                {
                    capacity = 0;
                }
                jobject.put("battery_capacity", capacity);
                
                
                jsonResult.add(jobject);


            }
            
            
        }
        else
        {
             JSONObject jobject = new JSONObject();
                jobject.put("command", "empty");
                 jsonResult.add(jobject);
        }
            
        
        return  jsonResult.toString();
    }


       @GetMapping("/availabledrone/{id}")
    String getAllAvailableDrone(@PathVariable int id)
    {
        
     // Optional<Medications> myMed = medinterface.findById(new Long(id));
      Optional<Medications> myMed = medinterface.findById(Long.valueOf(id));
     
      int weight = myMed.get().getWeight();
      
      List<Drones> myDrones = droneinterface.findAll();
      
      
        JSONArray jsonResult = new JSONArray();
        
       
        
        
        
        
        
        
        if (!myDrones.isEmpty())
        {
           
             for (Drones droneitem: myDrones)
            {
                
                int usedWeight = checkDroneWeight(droneitem.getId().intValue());
                int droneWeight = droneitem.getDroneWeight();
                
                int newWeight = weight + usedWeight;
                
                if (newWeight <= droneWeight)
                {
                   JSONObject jobject = new JSONObject();
              
                jobject.put("id", droneitem.getId());
                jobject.put("name", droneitem.getDroneName());
                jobject.put("serial_number", droneitem.getDroneSerialNumber());
                jobject.put("model", droneitem.getDroneModel());
                jobject.put("weight", droneitem.getDroneWeight());
               
                
              
                jobject.put("used_weight", usedWeight);
               
                jobject.put("state", droneitem.getDroneState());
                
                jobject.put("battery_timespan", droneitem.getBatteryTimeLimit());
                
                int capacity = getTimeDiff(droneitem.getLastRecharge().toString(), droneitem.getBatteryTimeLimit());
                if (capacity <= 0)
                {
                    capacity = 0;
                }
                jobject.put("battery_capacity", capacity);
                
                
                jsonResult.add(jobject);
                }
                
                
              


            }
            
        }
        else
        {
             JSONObject jobject = new JSONObject();
                jobject.put("command", "empty");
                 jsonResult.add(jobject);
        }
            
        
        return  jsonResult.toString();
    }
    
     @GetMapping("/drone/{id}")
    String getDroneInformation(@PathVariable Long id)
    {
      
       
        Optional<Drones> droneresult  = droneinterface.findById(id);
       
       
       
                JSONObject jobject = new JSONObject();
                jobject.put("command", "success");
                jobject.put("id", droneresult.get().getId());
                jobject.put("name", droneresult.get().getDroneName());
                jobject.put("serial_number", droneresult.get().getDroneSerialNumber());
                jobject.put("model", droneresult.get().getDroneModel());
                jobject.put("weight", droneresult.get().getDroneWeight());
                jobject.put("battery_capacity", 50);
                jobject.put("state", droneresult.get().getDroneState());
                
                
                
                
                
               


           
        
        return  jobject.toString();
    }
 
    
     @PostMapping("/drone")
    String AddDroneMed(@RequestParam int droneId, @RequestParam int medId, @RequestParam String name, @RequestParam int weight, @RequestParam String code, @RequestParam String image_url)
    {
        
        DroneMed dronmed = new DroneMed(droneId, medId, name, weight, code, image_url);
        
           int usedWeight = checkDroneWeight(droneId);
           int newWeight = usedWeight + weight;
         //  Optional <Drones> droneresult  = droneinterface.findById(new Long(droneId));
           Optional <Drones> droneresult  = droneinterface.findById(Long.valueOf(droneId));
           
         
         int droneWeight = droneresult.get().getDroneWeight();
           
          int capacity = getTimeDiff(droneresult.get().getLastRecharge().toString(), droneresult.get().getBatteryTimeLimit());
                if (capacity < 25)
                {
                     JSONObject jobject = new JSONObject();
                     jobject.put("operation", "add_medication");
                     jobject.put("status", "low_battery");
                     
                     return  jobject.toString();
                }
                else{
                    if (newWeight <= droneWeight )
           {
           
                dronemedinterface.save(dronmed);
                JSONObject jobject = new JSONObject();
                jobject.put("operation", "add_medication");
                jobject.put("status", "success");
               
             return  jobject.toString();    
           }
           
           else
           {
                JSONObject jobject = new JSONObject();
                jobject.put("operation", "add_medication");
                jobject.put("status", "no_space");
               
             return  jobject.toString();    
           }
                }
           
                
    }
    
     @PostMapping("/newdrone")
    String NewDrone(@RequestParam String droneName, @RequestParam String droneSerialNumber, @RequestParam String dronesModel, @RequestParam int droneWeight, @RequestParam int batteryTimeSpan)
    {
        
           droneModel dronemodel = droneModel.valueOf(dronesModel);
           java.util.Date now = new Date();
          Drones drones = new Drones(droneName, droneSerialNumber, dronemodel, droneWeight, batteryTimeSpan, now, stateEnum.IDLE);
        
          
                  droneinterface.save(drones);
                     JSONObject jobject = new JSONObject();
                     jobject.put("operation", "add_medication");
                     jobject.put("status", "success");
                     
                     return  jobject.toString();
              
                
           
                
    }
    
    
      @PostMapping("/newmed")
    String NewMed(@RequestParam String medName, @RequestParam int medWeight, @RequestParam String medCode, @RequestParam String medImage)
    {
        
          
          Medications meds = new Medications(medName, medWeight, medCode, medImage);
          
          
                  medinterface.save(meds);
                     JSONObject jobject = new JSONObject();
                     jobject.put("operation", "add_medication");
                     jobject.put("status", "success");
                     
                     return  jobject.toString();
              
                
           
                
    }
    
    
     @PutMapping("/drone")
    String changeDroneState(@RequestParam int droneId, @RequestParam String state)
    {
      final  stateEnum myState = stateEnum.valueOf(state);
      // Optional <Drones> droneresult  = droneinterface.findById(new Long(droneId));
       Optional <Drones> droneresult  = droneinterface.findById(Long.valueOf(droneId));
      
      
      int capacity = getTimeDiff(droneresult.get().getLastRecharge().toString(), droneresult.get().getBatteryTimeLimit());
                if (myState == stateEnum.LOADING  && capacity < 25)
                {
                     JSONObject jobject = new JSONObject();
                     jobject.put("operation", "change_state");
                     jobject.put("status", "low_battery");
                     
                     return  jobject.toString();
                }
                else
                {
                    
                         droneinterface.findById(Long.valueOf(droneId)).map(drone -> {
                         drone.setDroneState(myState);

                            return droneinterface.save(drone);
                        });
                          JSONObject jobject = new JSONObject();
                           jobject.put("operation", "change_state");
                           jobject.put("status", "success");
                           
                           return  jobject.toString();
                    
                }
      
      
    
    }
   
    
    @PutMapping("/dronerecharge")
    String rechargeDrone(@RequestParam int droneId)
    {
    
       java.util.Date now = new Date();
       droneinterface.findById(Long.valueOf(droneId)).map(drone -> {
                         drone.setLastRecharge(now);

                            return droneinterface.save(drone);
                        });
                          JSONObject jobject = new JSONObject();
                           jobject.put("operation", "drone_recharge");
                           jobject.put("status", "success");
                           
                           return  jobject.toString();
      
    
    }
   
    
     @DeleteMapping("/deleteAllMedication/{id}")
    String deleteAllMedication(@PathVariable int id)
    {
        List<DroneMed> myDrone = dronemedinterface.findByDroneId(id);
     
        JSONArray jsonResult = new JSONArray();
        
        if (!myDrone.isEmpty())
        {
            for (DroneMed meditem: myDrone)
            {
                
                dronemedinterface.delete(meditem);


            }
            
            
        }
                        JSONObject jobject = new JSONObject();
                           jobject.put("operation", "drone_recharge");
                           jobject.put("status", "success");
                           
                           return  jobject.toString();
        
       
    }
    
       @DeleteMapping("/deleteAllMedicationDrone/{id}")
    String deleteAllMedicationDrone(@PathVariable int id)
    {
        List<DroneMed> myDrone = dronemedinterface.findByMedId(id);
     
        JSONArray jsonResult = new JSONArray();
        
        if (!myDrone.isEmpty())
        {
            for (DroneMed meditem: myDrone)
            {
                
                dronemedinterface.delete(meditem);


            }
            
            
        }
                        JSONObject jobject = new JSONObject();
                           jobject.put("operation", "drone_recharge");
                           jobject.put("status", "success");
                           
                           return  jobject.toString();
        
       
    }
    
    
    @DeleteMapping("/deleteDroneInformation/{id}")
    String deleteDroneInformation(@PathVariable int id)
    {
        Optional<Drones> myDrone = droneinterface.findById(Long.valueOf(id));
     
        JSONArray jsonResult = new JSONArray();
         droneinterface.delete(myDrone.get());
         
        List<DroneMed> droneInfo = dronemedinterface.findByDroneId(id);
     
       
        
        if (!droneInfo.isEmpty())
        {
            for (DroneMed droneItem: droneInfo)
            {
                
                dronemedinterface.delete(droneItem);


            }
            
            
        }
      
          JSONObject jobject = new JSONObject();
          jobject.put("operation", "drone_recharge");
          jobject.put("status", "success");
          return  jobject.toString();
        
       
    }
    
    
    @DeleteMapping("/deleteMedicationInformation/{id}")
    String deleteMedicationInformation(@PathVariable int id)
    {
        Optional<Medications> myMedication = medinterface.findById(Long.valueOf(id));
     
        JSONArray jsonResult = new JSONArray();
         medinterface.delete(myMedication.get());
         
        List<DroneMed> medinfo = dronemedinterface.findByMedId(id);
     
       
        
        if (!medinfo.isEmpty())
        {
            for (DroneMed medItem: medinfo)
            {
                
                dronemedinterface.delete(medItem);


            }
            
            
        }
      
          JSONObject jobject = new JSONObject();
          jobject.put("operation", "drone_recharge");
          jobject.put("status", "success");
          return  jobject.toString();
        
       
    }
    
    
    
    
    @DeleteMapping("/drone/{id}")
    String deleteDrone(@PathVariable Long id)
    {
         droneinterface.deleteById(id);
         JSONObject jobject = new JSONObject();
         jobject.put("command", "success");
         return  jobject.toString();
         
    }
 
    
    
     public int checkDroneWeight(int drone_id)
    {
        
   
        List<DroneMed> myDrone = dronemedinterface.findByDroneId(drone_id);
       
        int weight = 0;
        
        if (!myDrone.isEmpty())
        {
            for (int i = 0; i < myDrone.size(); i++)
                {
                
                    weight += myDrone.get(i).getWeight();
                }
                    
                
        }
       
        return weight;
       

        
        
    }
    
    public int getTimeDiff(String last_recharge, int battery_span)
    {
        String dateStart = last_recharge;
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");  

        java.util.Date d1 = null;
        java.util.Date d2 = new Date();
        try {
            d1 = format.parse(dateStart);

        } catch (ParseException e) {
            e.printStackTrace();
        }    

        long diff = d2.getTime() - d1.getTime();
        long diffSeconds = diff / 1000;         
        long diffMinutes = diff / (60 * 1000);         
        long diffHours = diff / (60 * 60 * 1000);                      
        float result = battery_span - diffMinutes;
        result /=  battery_span;
        result *=  100;
        return (int)result;

    }
    
    
    
}
