/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft.Interface;

import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Drones;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;

/**
 *
 * @author arotob
 */
public interface DrioneInterface extends JpaRepository<Drones, Long>{
    
      List<Drones> findBydroneSerialNumber(String serial_number);
    
}
