/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft.Interface;

import com.arowolo.oluwatobi.MusalaSoft.ClassModels.Medications;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author arotob
 */
public interface MedicationInterface extends JpaRepository<Medications, Long>{
    
}
