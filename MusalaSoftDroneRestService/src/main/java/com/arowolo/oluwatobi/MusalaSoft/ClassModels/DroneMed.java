/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arowolo.oluwatobi.MusalaSoft.ClassModels;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author arotob
 */
@Entity
public class DroneMed {
    private @Id @GeneratedValue Long id;
    private int droneId;
    private int medId;
    private String name;
    private int weight;
    private String code;
    private String image_url;
    
    
    
    
    public DroneMed()
    {
        
    }
    public  DroneMed(int drone_id, int med_id, String name, int weight, String code, String image_url)
    {
        this.droneId = drone_id;
        this.medId = med_id;
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.image_url = image_url;
        
        
        
    }
    
    public Long getId ()
    {
        return this.id;
    }
     public int getDroneId ()
    {
        return this.droneId;
    }
      public int getMedId()
    {
        return this.medId;
    }
    public String getName()
    {
        return this.name;
        
    }
    public int getWeight()
    {
        return this.weight;
    }
    public String getCode()
    {
        return this.code;
    }
    public String getImageUrl()
    {
        return this.image_url;
    }
            
    
    
}
