/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MusalaSoftDroneApp.models;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author arotob
 */
public class medicationModel {
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("code")
    public String code;
    @SerializedName("weight")
    public int weight;
    @SerializedName("image_url")
    public String image_url;
    
    
    
}
