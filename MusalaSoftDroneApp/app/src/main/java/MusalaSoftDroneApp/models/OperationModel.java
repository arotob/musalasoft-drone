/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MusalaSoftDroneApp.models;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author arotob
 */
public class OperationModel {
    @SerializedName("operation")
    public String operation;
    @SerializedName("status")
    public String status;
    
    
}
