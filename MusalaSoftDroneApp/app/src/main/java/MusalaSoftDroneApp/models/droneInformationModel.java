/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MusalaSoftDroneApp.models;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author arotob
 */
public class droneInformationModel {
    
    
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("weight")
    public String weight;
    @SerializedName("serial_number")
    public String serial_number;
    @SerializedName("model")
    public String model;
    
    @SerializedName("battery_capacity")
    public String battery_capacity;
    
    @SerializedName("battery_timespan")
    public String battery_timespan;
    
    @SerializedName("state")
    public String state;
    
    @SerializedName("used_weight")
    public String used_weight;
   
    
    
    
}
