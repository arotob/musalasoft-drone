/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package MusalaSoftDroneApp.APIService;

import MusalaSoftDroneApp.models.OperationModel;
import MusalaSoftDroneApp.models.droneInformationModel;
import MusalaSoftDroneApp.models.droneMedModel;
import MusalaSoftDroneApp.models.medicationModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 *
 * @author arotob
 */
public interface APIInterface {
    
    
   
    @GET("/drone/alls")
    Call<List<droneInformationModel>>doGetAllDroneInformation();
    
    @GET("/medication/alls")
    Call<List<medicationModel>>doGetAllMedicationInfornation();
    
  
    @GET("/dronemedication/{id}")
     Call<List<droneMedModel>>doGetDroneMedicationInformation(@Path(value = "id", encoded = true) int droneId);

    @GET("/medicationdrone/{id}")
     Call<List<droneInformationModel>>doGetMedicationDroneInformation(@Path(value = "id", encoded = true) int medId);

     
    @GET("/availabledrone/{id}")
     Call<List<droneInformationModel>>doGetAvialableMedicationDroneInformation(@Path(value = "id", encoded = true) int medId);

 
    @POST("/drone")
    @FormUrlEncoded
    Call<OperationModel>doAddMedicationDrone(@Field("droneId") int droneId, @Field("medId") int medId, @Field("name") String name, @Field("weight") int weight, @Field("code") String code, @Field("image_url") String image_url );
    
   
    
    @POST("/newdrone")
    @FormUrlEncoded
    Call<OperationModel>doAddNewDrone(@Field("droneName") String droneName, @Field("droneSerialNumber") String  droneSerialNumber, @Field("dronesModel") String dronesModel, @Field("droneWeight") int droneWeight, @Field("batteryTimeSpan") int batteryTimeSpan);
    
    @POST("/newmed")
    @FormUrlEncoded
    Call<OperationModel>doAddNewMedication(@Field("medName") String medName, @Field("medWeight") int medWeight, @Field("medCode") String medCode, @Field("medImage") String medImage);
    
    
    @PUT("/drone")
    @FormUrlEncoded
    Call<OperationModel>doChangeDroneState(@Field("droneId") int droneId, @Field("state") String state);
    
    @PUT("/dronerecharge")
    @FormUrlEncoded
    Call<OperationModel>doRechargeBattery(@Field("droneId") int droneId);
    
    
    @DELETE("/deleteAllMedication/{id}")
    Call<OperationModel>doDeleteAllDroneMedication(@Path(value = "id", encoded = true) int droneId);
    
  
    @DELETE("/deleteAllMedicationDrone/{id}")
    Call<OperationModel>doeleteAllMedicationDrone(@Path(value = "id", encoded = true) int medId);
    
    
     @DELETE("/deleteDroneInformation/{id}")
    Call<OperationModel>deleteDroneInformation(@Path(value = "id", encoded = true) int droneId);
    
    
     @DELETE("/deleteMedicationInformation/{id}")
    Call<OperationModel>deleteMedicationInformation(@Path(value = "id", encoded = true) int medId);
    
  
    

    
}
